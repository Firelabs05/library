<?php
require_once ('../core/config.php');

class author_model extends database {
    private $name;
    private $id;
    private $born_date;
    private $die_date;
    private $nationality;
    
    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_name($name){
        $this->name = $name;
    }
    public function get_name(){
        return $this->name;
    }
    public function set_id($id){
        $this->id = $id;
    }
    public function get_id(){
        return $this->id;
    }
    public function set_born_date($born_date){
        $this->born_date = $born_date;
    }
    public function get_born_date(){
        return $this->born_date;
    }
        public function set_die_date($die_date){
        $this->die_date = $die_date;
    }
    public function get_die_date(){
        return $this->die_date;
    }
    public function set_nationality($nationality){
        $this->nationality = $nationality;
    }
    public function get_nationality(){
        return $this->nationality;
    }
    public function get_data(){
        return $this->data;
    }
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################

    public function set(){
        $this->sql = "
        INSERT INTO     author SET 
        name = '".$this->get_name()."'";
        if($this->get_born_date()){
            $this->sql .= ", born_date = '".$this->get_born_date()."'";
        } 
        if($this->get_die_date()){
            $this->sql .= ",die_date = '".$this->get_die_date()."'";
        }
        if($this->get_nationality()){
            $this->sql .= ", nationality = '".$this->get_nationality()."'";
        }
        $this->IUDquery();
    }
    public function delete(){
        $this->sql = "
        DELETE FROM author
        WHERE           id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function edit(){
        $this->sql = "
        SELECT * FROM author
        WHERE       id ='".$this->get_id()."'";
        $this->Squery();
    }
    public function update(){
        $this->sql = "
        UPDATE author SET
        name = '".$this->get_name()."'";
        if($this->get_born_date()){
            $this->sql .= ", born_date = '".$this->get_born_date()."'";
        } 
        if($this->get_die_date()){
            $this->sql .= ",die_date = '".$this->get_die_date()."'";
        }
        if($this->get_nationality()){
            $this->sql .= ", nationality = '".$this->get_nationality()."'";
        }
        $this->sql .= "WHERE   id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function get(){
        $this->sql = "
        SELECT * FROM author
        WHERE         name LIKE '%".$this->get_name()."%'";
        $this->Squery();
    }
    ##################### FIN #########################
    
    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct(){
        $this->db_name = 'library';
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################
}
?>