<?php
require_once ('author_model.php');
require_once('../core/constant.php');
require_once('view_author.php');
function start(){
    $request = URL_ADD;
    $url = $_SERVER['REQUEST_URI'];
    $events = [VIEW_ADD, SET, DELETE,
               SHOW_EDIT, URL_MODIFY, URL_ADD,
               URL_EDIT];
    $models = [AUTHOR];

    foreach ($events as $event) {
        foreach ($models as $model) {
            $url_event = $model.$event;
            if(strpos($url,$url_event) == true){
                $request = $url_event;
            }
        }
    }
    $obj_author = obj();
    $data = helper_author();
    switch ($request){
        case AUTHOR.URL_ADD:
            if(isset($data['name'])){
                $obj_author->set_name($data['name']);
                if(isset($data['born_date'])){
                    $obj_author->set_born_date($data['born_date']);
                }
                if(isset($data['die_date'])){
                    $obj_author->set_die_date($data['die_date']);
                }
                if(isset($data['nationality'])){
                    $obj_author->set_nationality($data['nationality']);
                }                
                $obj_author->set();
                return_view(URL_ADD);
            }else{
                return_view(URL_ADD);
            }
        break;
        case AUTHOR.URL_MODIFY:
            if(isset($data['id']) && isset($data['name']) ){
                $obj_author->set_name($data['name']);
                $obj_author->set_id($data['id']);
                if(isset($data['born_date'])){
                    $obj_author->set_born_date($data['born_date']);
                }
                if(isset($data['die_date'])){
                    $obj_author->set_die_date($data['die_date']);
                }
                if(isset($data['nationality'])){
                    $obj_author->set_nationality($data['nationality']);
                }        
                $obj_author->update();
                $obj_author->set_name('');
                $obj_author->get();
                return_view(URL_MODIFY,$obj_author->get_data());
            }else if(isset($data['name'])){
                $obj_author->set_name($data['name']);
                $obj_author->get();
                return_view(URL_MODIFY,$obj_author->get_data());
            }else{
                $obj_author->set_name('');
                $obj_author->get();
                return_view(URL_MODIFY,$obj_author->get_data());
            }
        break;
        case AUTHOR.URL_EDIT:
            if(isset($data['id'])){
                $obj_author->set_id($data['id']);
                $obj_author->edit();
                return_view(URL_EDIT,$obj_author->get_data());
            }else if(isset($data['id_delete'])){    
                $obj_author->set_id($data['id_delete']);
                $obj_author->delete();
                $obj_author->set_name('');
                $obj_author->get();
                return_view(URL_MODIFY,$obj_author->get_data());
            }else{
                $obj_author->set_name('');
                $obj_author->get();
                return_view(URL_MODIFY,$obj_author->get_data());
            }
        break;
        default:
            return_view($request);
        break;
    }
}
function obj(){
    $obj = new author_model();
    return $obj;
}
function helper_author(){
    $array = [];
    if($_POST){
        if(isset($_POST['name']) && $_POST['name'] != ''){
            $array['name'] = $_POST['name'];
        }
        if(isset($_POST['id']) && $_POST['id'] != ''){
            $array['id'] = $_POST['id'];
        }
        if(isset($_POST['id_delete']) && $_POST['id_delete'] != ''){
            $array['id_delete'] = $_POST['id_delete'];
        }
        if(isset($_POST['born_date']) && $_POST['born_date'] != ''){
            $array['born_date'] = $_POST['born_date'];
        }
        if(isset($_POST['die_date']) && $_POST['die_date'] != ''){
            $array['die_date'] = $_POST['die_date'];
        }
        if(isset($_POST['nationality']) && $_POST['nationality'] != ''){
            $array['nationality'] = $_POST['nationality'];
        }       
    }
    return $array;
}
start();
?>