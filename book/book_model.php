<?php
require_once ('../core/config.php');
class book_model extends database {
    private $name;
    private $id;
    private $description;
    private $number_page;
    private $lenguage;
    private $priority;
    private $edition;
    private $publication_date;
    private $publishing_house;
    private $publication_country;
    private $author;
    private $gender;

    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_name($name){
        $this->name = $name;
    }
    public function get_name(){
        return $this->name;
    }
    public function set_id($id){
        $this->id = $id;
    }
    public function get_id(){
        return $this->id;
    }
    public function get_last_id(){
        return $this->last_id;
    }
    public function set_description($description){
        $this->description = $description;
    }
    public function get_description(){
        return $this->description;
    }
    public function set_number_page($number_page){
        $this->number_page = $number_page;
    }
    public function get_number_page(){
        return $this->number_page;
    }
    public function set_language($lenguage){
        $this->lenguage = $lenguage;
    }
    public function get_language(){
        return $this->lenguage;
    }   
    public function set_priority($priority){
        $this->priority = $priority;
    }
    public function get_priority(){
        return $this->priority;
    }
    public function set_edition($edition){
        $this->edition = $edition;
    }
    public function get_edition(){
        return $this->edition;
    }     
    public function set_publication_date($publication_date){
        $this->publication_date = $publication_date;
    }
    public function get_publication_date(){
        return $this->publication_date;
    }      
    public function set_publishing_house($publishing_house){
        $this->publishing_house = $publishing_house;
    }
    public function get_publishing_house(){
        return $this->publishing_house;
    }   
    public function set_publication_country($publication_country){
        $this->publication_country = $publication_country;
    }
    public function get_publication_country(){
        return $this->publication_country;
    }      
    public function get_data(){
        return $this->data;
    }
    public function set_data($data){
        $this->data =$data;
    }
    public function set_file($file){
        $this->file = $file;
    }
    public function get_file(){
        return $this->file;
    }
    public function set_author($author){
        $this->author = $author;
    }
    public function get_author(){
        return $this->author;
    }
    public function set_gender($gender){
        $this->gender = $gender;
    }
    public function get_gender(){
        return $this->gender;
    }    
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################

    public function set(){
        $this->sql = "
        INSERT INTO     books SET 
        name = '".$this->get_name()."',
        description = '".$this->get_description()."',
        number_page = '".$this->get_number_page()."',
        language = '".$this->get_language()."',
        priority = '".$this->get_priority()."',
        publishing_house = '".$this->get_publishing_house()."'";
        if(strpos('.jpg',$this->get_name_file())){
            $this->sql .= ", front = '".$this->get_name_file()."'";
        } 
        if($this->get_edition()){
            $this->sql .= ",edition = '".$this->get_edition()."'";
        }
        if($this->get_publication_date()){
            $this->sql .= ", publication_date = '".$this->get_publication_date()."'";
        }
        if($this->get_publication_country()){
            $this->sql .= ", publication_country = '".$this->get_publication_country()."'";
        }
        $this->IUDquery();
    }
    public function set_author_book(){
        $this->sql = "
        INSERT INTO author_books SET
        book = '".$this->get_id()."',
        author = '".$this->get_author()."'";
        $this->IUDquery();
    }
    public function set_gender_book(){
        $this->sql = "
        INSERT INTO gender_books SET
        book = '".$this->get_id()."',
        gender = '".$this->get_gender()."'";
        $this->IUDquery();
    }    
    public function delete(){
        $this->sql = "
        DELETE FROM books
        WHERE           id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function edit(){
        $this->sql = "
        SELECT * FROM books
        WHERE       id ='".$this->get_id()."'";
        $this->Squery();
    }
    public function update(){
        $this->sql = "
        UPDATE books SET
        name = '".$this->get_name()."',
        description = '".$this->get_description()."',
        number_page = '".$this->get_number_page()."',
        language = '".$this->get_language()."',
        priority = '".$this->get_priority()."',
        publishing_house = '".$this->get_publishing_house()."'";
        if(strpos('.jpg',$this->get_name_file())){
            $this->sql .= ", front = '".$this->get_name_file()."'";
        } 
        if($this->get_edition()){
            $this->sql .= ",edition = '".$this->get_edition()."'";
        }
        if($this->get_publication_date()){
            $this->sql .= ", publication_date = '".$this->get_publication_date()."'";
        }
        if($this->get_publication_country()){
            $this->sql .= ", publication_country = '".$this->get_publication_country()."'";
        }
        $this->sql .= " WHERE   id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function get(){
        $this->sql = "
        SELECT b.id, b.name, b.description , b.front, b.number_page, 
        b.language, b.priority, b.edition, b.publication_date,
        ph.name AS publishing_house, b.publication_country 
        FROM books AS b INNER JOIN
        publishing_house AS ph ON
        b.name LIKE '%".$this->get_name()."%'
        AND b.publishing_house = ph.id";
        $this->Squery();
    }
    public function get_author_db(){
        $this->sql = "
        SELECT id, name FROM author";
        $this->Squery();
    }
    public function get_gender_db(){
        $this->sql = "
        SELECT id, name FROM genders";
        $this->Squery();
    }
    public function get_house(){
        $this->sql = "
        SELECT id, name FROM publishing_house";
        $this->Squery();
    }
    public function update_file(){
        $this->info_extension();
        $this->new_name();
        $this->move_file();
    }
    ##################### FIN #########################
    
    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct($db_name ='', $target_dir =''){
        if ($db_name != ''){
            $this->db_name = $db_name;
        }else{
            $this->db_name = 'library';
        }
        
        if($target_dir != ''){
            $this->target_dir = $target_dir;
        }else {
            $this->target_dir = "C:/wamp64/www/library/Fronts/";
        } 
        
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################
}
?>