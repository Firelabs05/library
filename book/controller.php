<?php
require_once ('book_model.php');
require_once('../core/constant.php');
require_once('view_book.php');
function start(){
    $request = URL_ADD;
    $url = $_SERVER['REQUEST_URI'];
    $events = [VIEW_ADD, SET, DELETE,
               SHOW_EDIT, URL_MODIFY, URL_ADD,
               URL_EDIT];
    $models = [BOOK];

    foreach ($events as $event) {
        foreach ($models as $model) {
            $url_event = $model.$event;
            if(strpos($url,$url_event) == true){
                $request = $url_event;
            }
        }
    }
    $obj_book = obj();
    $data = helper_book();
    $select = [];
    switch ($request){
        case BOOK.URL_ADD:
            if(isset($data['name']) && isset($data['publishing_house']) 
            && isset($data['description']) && isset($data['number_page'])
            && isset($data['language']) && isset($data['priority']) 
            && count($data['author'])>0 && count($data['gender'])>0 ){
                $obj_book->set_name($data['name']);
                $obj_book->set_publishing_house($data['publishing_house']);
                $obj_book->set_description($data['description']);
                $obj_book->set_number_page($data['number_page']);
                $obj_book->set_language($data['language']);
                $obj_book->set_priority($data['priority']);
                if(isset($_FILES['front'])){
                    $obj_book->set_file('front');
                    $obj_book->update_file();
                }
                if(isset($data['edition'])){
                    $obj_book->set_edition($data['edition']);
                }
                if(isset($data['publication_date'])){
                    $obj_book->set_publication_date($data['publication_date']);
                }  
                if(isset($data['publication_country'])){
                    $obj_book->set_publication_country($data['publication_country']);
                }                        
                $obj_book->set();
                $obj_book->set_id($obj_book->get_last_id());
                foreach ($data['author'] as $key ) {
                    $obj_book->set_author($key);
                    $obj_book->set_author_book();
                }
                foreach ($data['gender'] as $key ) {
                    $obj_book->set_gender($key);
                    $obj_book->set_gender_book();
                }
            }
            $obj_book->get_author_db();
            $select['author'] = $obj_book->get_data();
            $obj_book->set_data(null);
            $obj_book->get_gender_db();
            $select['gender'] = $obj_book->get_data();
            $obj_book->set_data(null);
            $obj_book->get_house();
            $select['house'] = $obj_book->get_data();
            return_view(URL_ADD, $select);
        break;
        case BOOK.URL_MODIFY:
           if(isset($data['name']) && isset($data['publishing_house']) 
            && isset($data['description']) && isset($data['number_page'])
            && isset($data['language']) && isset($data['priority']) 
            && count($data['author'])>0 && count($data['gender'])>0 ){
                $obj_book->set_name($data['name']);
                $obj_book->set_id($data['id']);
                $obj_book->set_publishing_house($data['publishing_house']);
                $obj_book->set_description($data['description']);
                $obj_book->set_number_page($data['number_page']);
                $obj_book->set_language($data['language']);
                $obj_book->set_priority($data['priority']);
                if(isset($_FILES['front'])){
                    $obj_book->set_file('front');
                    $obj_book->update_file();
                }
                if(isset($data['edition'])){
                    $obj_book->set_edition($data['edition']);
                }
                if(isset($data['publication_date'])){
                    $obj_book->set_publication_date($data['publication_date']);
                }  
                if(isset($data['publication_country'])){
                    $obj_book->set_publication_country($data['publication_country']);
                }           
                $obj_book->update();
                ###### TODAVIA ESTOY PENDEJO Y TENGO VARIAS DUDAS ACA ######
               /* $obj_book->set_id($obj_book->get_last_id());
                foreach ($data['author'] as $key ) {
                    $obj_book->set_author($key);
                    $obj_book->set_author_book();
                }
                foreach ($data['gender'] as $key ) {
                    $obj_book->set_gender($key);
                    $obj_book->set_gender_book();
                } */  //
                $obj_book->set_name('');
                $obj_book->get();
                return_view(URL_MODIFY,$obj_book->get_data());
            }else if(isset($data['name'])){
                $obj_book->set_name($data['name']);
                $obj_book->get();
                return_view(URL_MODIFY,$obj_book->get_data());
            }else{
                $obj_book->set_name('');
                $obj_book->get();
                return_view(URL_MODIFY,$obj_book->get_data());
            }
        break;
        case BOOK.URL_EDIT:
            if(isset($data['id'])){
                $obj_book->set_id($data['id']);
                $obj_book->edit();
                $select = $obj_book->get_data();
                $obj_book->set_data(null);
                $obj_book->get_author_db();
                $select['author'] = $obj_book->get_data();
                $obj_book->set_data(null);
                $obj_book->get_gender_db();
                $select['gender'] = $obj_book->get_data();
                $obj_book->set_data(null);
                $obj_book->get_house();
                $select['house'] = $obj_book->get_data();
                return_view(URL_EDIT,$select);
            }else if(isset($data['id_delete'])){    
                $obj_book->set_id($data['id_delete']);
                $obj_book->delete();
                $obj_book->set_name('');
                $obj_book->get();
                return_view(URL_MODIFY,$obj_book->get_data());
            }else{
                $obj_book->set_name('');
                $obj_book->get();
                return_view(URL_MODIFY,$obj_book->get_data());
            }
        break;
        default:
            return_view($request);
        break;
    }
}
function obj(){
    $obj = new book_model();
    return $obj;
}
function helper_book(){
    $array = [];
    if($_POST){
        if(isset($_POST['name']) && $_POST['name'] != ''){
            $array['name'] = $_POST['name'];
        }
        if(isset($_POST['id']) && $_POST['id'] != ''){
            $array['id'] = $_POST['id'];
        }
        if(isset($_POST['id_delete']) && $_POST['id_delete'] != ''){
            $array['id_delete'] = $_POST['id_delete'];
        }
        if(isset($_POST['description']) && $_POST['description'] != ''){
            $array['description'] = $_POST['description'];
        }
        if(isset($_POST['number_page']) && $_POST['number_page'] != ''){
            $array['number_page'] = $_POST['number_page'];
        }
        if(isset($_POST['priority']) && $_POST['priority'] != ''){
            $array['priority'] = $_POST['priority'];
        }
        if(isset($_POST['language']) && $_POST['language'] != ''){
            $array['language'] = $_POST['language'];
        }
        if(isset($_POST['edition']) && $_POST['edition'] != ''){
            $array['edition'] = $_POST['edition'];
        }
        if(isset($_POST['publication_date']) && $_POST['publication_date'] != ''){
            $array['publication_date'] = $_POST['publication_date'];
        }   
        if(isset($_POST['publishing_house']) && $_POST['publishing_house'] != ''){
            $array['publishing_house'] = $_POST['publishing_house'];
        }   
        if(isset($_POST['publication_country']) && $_POST['publication_country'] != ''){
            $array['publication_country'] = $_POST['publication_country'];
        }
        if(isset($_POST['author1'])){
            $array['author'] = [];
            for($c = 1; $c<=4; $c++){
                if($_POST['author'.$c] != ''){
                    $array['author'][] = $_POST['author'.$c];
                }
            }
        }
        if(isset($_POST['gender1'])){
            $array['gender'] = [];
            for($c = 1; $c<=4; $c++){
                if($_POST['gender'.$c] != ''){
                    $array['gender'][] = $_POST['gender'.$c];
                }
            }
        }        
    }
    return $array;
}
start();
?>