<?php 
$dictionary = [
    'form' => [
        URL_ADD => "/library/".BOOK.URL_ADD,
        URL_MODIFY => "/library/".BOOK.URL_MODIFY,
        URL_EDIT => "/library/".BOOK.URL_EDIT
    ],
    'a_href' => [
        'house_url_'.URL_ADD => PUBLISHING_HOUSE.URL_ADD.'/',
        'house_url_'.URL_MODIFY => PUBLISHING_HOUSE.URL_MODIFY.'/',
        'gender_url_'.URL_ADD => GENDER.URL_ADD.'/',
        'gender_url_'.URL_MODIFY => GENDER.URL_MODIFY.'/',
        'author_url_'.URL_ADD => AUTHOR.URL_ADD.'/',
        'author_url_'.URL_MODIFY => AUTHOR.URL_MODIFY.'/',
        'book_url_'.URL_ADD => BOOK.URL_ADD.'/',
        'book_url_'.URL_MODIFY => BOOK.URL_MODIFY.'/'
    ],
     'general' => [
        'subtitle' => 'Libros'
    ]
];

function get_template($dir){
    $file = '../site_media/html/'.$dir.'.html';
    $html = file_get_contents($file);
    return $html;
}

function replace($html,$array = []){
    foreach ($array as $key => $value) {
        $html = str_replace('{'.$key.'}',$value,$html);
    }
    return $html;
}
function return_view($dir,$data = []){
    global $dictionary;
    $html = get_template('base');
    $html = replace ($html,$dictionary['general']);
    $html = str_replace('{form}',get_template($dir.'_book'),$html);
    $html = replace($html,$dictionary['a_href']);
    $html = replace($html,$dictionary['form']);
    if($dir == URL_MODIFY){
        $html = str_replace('{content}',dinamic_table_edit($data),$html);    
    }
    if(array_key_exists('author',$data)){
        $html = str_replace('{select_author}',dinamic_option($data['author']),$html);
    }
    if(array_key_exists('gender',$data)){
        $html = str_replace('{select_gender}',dinamic_option($data['gender']),$html);
    }
    if(array_key_exists('house',$data)){
        $html = str_replace('{select_house}',dinamic_option($data['house']),$html);
    }
    if($dir == URL_EDIT){
        $html = replace($html,$data[0]);
    }
    print($html);
}
function dinamic_table_edit($data = []){
    $table ='';
    foreach ($data as $key) {
        $table .='<tr>';
        foreach ($key as $key2) {
            if($key2 == $key['front'] && $key2 != null){
                $table .= '<td><img class = "col-md-12" src ="/library/Fronts/'.$key2.'"></td>';
            }
            else{
                $table.='<td>'.$key2.'</td>';
            }
            
        }
        $table .= '<td>
                <button type="submit" class="btn btn-info" name = "id" value ="'.$key['id'].'">Mod</button>
                <button type="submit" class="btn btn-danger" name ="id_delete" value ="'.$key['id'].'">Eli</button>
                </td>';
        $table .='</tr>';
    }
    return $table;
}
function dinamic_option($data =[]){
    $option ='';
    $option.='<option value = ""></option>';
    foreach ($data as $key) {
        $option.='<option value = "'.$key['id'].'">'.$key['name'].'</option>';
    }
    return $option;
}
?>