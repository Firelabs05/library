<?php
require_once('config_file.php');
abstract class database extends file{
    private static $db_host = 'localhost';
    private static $db_user = 'admin_library';
    private static $db_pass = 'library';

    protected $db_name = '';
    protected $data = array();
    protected $sql;
    protected $last_id;

    private $link;

    abstract protected function get();
    abstract protected function set();
    abstract protected function edit();
    abstract protected function delete();

    protected function connection() {
        try {
            $this->link = new mysqli(self::$db_host,self::$db_user,
                                     self::$db_pass,$this->db_name);
            $tilde = $this->link->query("SET NAMES 'utf8'");
        }catch (mysql_error $error){
            echo 'Error al conectarse con la base de datos'.$error->getMessage();
        }
    }
    protected function disconnect(){
        $this->link->close();
    }
    protected function IUDquery(){
        $this->connection();
        if (!$this->link->query($this->sql)) {
            printf("Errormessage: %s\n", $this->link->error);
        }
        $this->last_id = $this->link->insert_id;
        $this->disconnect();
    }
    protected function Squery(){
        $this->connection();
        $result = $this->link->query($this->sql);
        while($this->data[] = $result->fetch_assoc());
        $result->close();
        $this->disconnect();
        array_pop($this->data);
    }
}

?>