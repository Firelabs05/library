<?php

abstract class file {
    protected $target_dir;
    protected $extension;
    protected $name_file;
    protected $file;

    protected function info_extension(){
        $this->extension = pathinfo(basename($_FILES[$this->get_file()]["name"]),PATHINFO_EXTENSION);
    }
    protected function new_name(){
        $this->name_file =uniqid(). '.'.$this->extension;
    }
    protected function move_file(){
        move_uploaded_file($_FILES[$this->get_file()]["tmp_name"], $this->target_dir . $this->name_file);
    }
    protected function get_name_file(){
        return $this->name_file;
    }
}
?>