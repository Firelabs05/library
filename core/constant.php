<?php
#MODULOS
const PUBLISHING_HOUSE = 'publishing_house/';
const GENDER = 'gender/';
const AUTHOR = 'author/';
const BOOK = 'book/';

#GENERAL VISTAS
const VIEW_ADD = "new";
#GENERAL ACCIONES
const SET = 'set';
const DELETE = 'delete';
const SHOW_EDIT = 'show_edit';


#FORMULARIOS
const URL_MODIFY = 'modify';
const URL_ADD = 'add';
const URL_EDIT = 'edit';
?>