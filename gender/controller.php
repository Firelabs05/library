<?php
require_once ('gender_model.php');
require_once('../core/constant.php');
require_once('view_gender.php');
function start(){
    $request = URL_ADD;
    $url = $_SERVER['REQUEST_URI'];
    $events = [VIEW_ADD, SET, DELETE,
               SHOW_EDIT, URL_MODIFY, URL_ADD,
               URL_EDIT];
    $models = [GENDER];

    foreach ($events as $event) {
        foreach ($models as $model) {
            $url_event = $model.$event;
            if(strpos($url,$url_event) == true){
                $request = $url_event;
            }
        }
    }
    $obj_gender = obj();
    $data = helper_gender();
    switch ($request){
        case GENDER.URL_ADD:
            if(isset($data['name']) && isset($data['description'])){
                $obj_gender->set_name($data['name']);
                $obj_gender->set_description($data['description']);
                $obj_gender->set();
                return_view(URL_ADD);
            }else{
                return_view(URL_ADD);
            }
        break;
        case GENDER.URL_MODIFY:
            if(isset($data['id']) && isset($data['name']) && isset($data['description'])){
                $obj_gender->set_name($data['name']);
                $obj_gender->set_id($data['id']);
                $obj_gender->set_description($data['description']);
                $obj_gender->update();
                $obj_gender->set_name('');
                $obj_gender->get();
                return_view(URL_MODIFY,$obj_gender->get_data());
            }else if(isset($data['name'])){
                $obj_gender->set_name($data['name']);
                $obj_gender->get();
                return_view(URL_MODIFY,$obj_gender->get_data());
            }else{
                $obj_gender->set_name('');
                $obj_gender->get();
                return_view(URL_MODIFY,$obj_gender->get_data());
            }
        break;
        case GENDER.URL_EDIT:
            if(isset($data['id'])){
                $obj_gender->set_id($data['id']);
                $obj_gender->edit();
                return_view(URL_EDIT,$obj_gender->get_data());
            }else if(isset($data['id_delete'])){    
                $obj_gender->set_id($data['id_delete']);
                $obj_gender->delete();
                $obj_gender->set_name('');
                $obj_gender->get();
                return_view(URL_MODIFY,$obj_gender->get_data());
            }else{
                $obj_gender->set_name('');
                $obj_gender->get();
                return_view(URL_MODIFY,$obj_gender->get_data());
            }
        break;
        default:
            return_view($request);
        break;
    }
}
function obj(){
    $obj = new gender_model();
    return $obj;
}
function helper_gender(){
    $array = [];
    if($_POST){
        if(isset($_POST['name']) && $_POST['name'] != ''){
            $array['name'] = $_POST['name'];
        }
        if(isset($_POST['id']) && $_POST['id'] != ''){
            $array['id'] = $_POST['id'];
        }
        if(isset($_POST['id_delete']) && $_POST['id_delete'] != ''){
            $array['id_delete'] = $_POST['id_delete'];
        }
        if(isset($_POST['description']) && $_POST['description'] != ''){
            $array['description'] = $_POST['description'];
        }
    }
    return $array;
}
start();
?>