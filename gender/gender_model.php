<?php
require_once ('../core/config.php');

class gender_model extends database {
    private $name;
    private $id;
    private $description;
    
    ######## OBTENER Y INTRODUCIR DATOS A LAS PROPIEDADES ############
    public function set_name($name){
        $this->name = $name;
    }
    public function get_name(){
        return $this->name;
    }
    public function set_id($id){
        $this->id = $id;
    }
    public function get_id(){
        return $this->id;
    }
    public function set_description($description){
        $this->description = $description;
    }
    public function get_description(){
        return $this->description;
    }
    public function get_data(){
        return $this->data;
    }
    ####################### FIN ######################################

    #######################METODOS PRINCIPALES ##########################
    public function set(){
        $this->sql = "
        INSERT INTO     genders SET 
        name = '".$this->get_name()."', 
        description = '".$this->get_description()."'";
        $this->IUDquery();
    }
    public function delete(){
        $this->sql = "
        DELETE FROM genders
        WHERE           id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function edit(){
        $this->sql = "
        SELECT * FROM genders
        WHERE       id ='".$this->get_id()."'";
        $this->Squery();
    }
    public function update(){
        $this->sql = "
        UPDATE genders SET
        name = '".$this->get_name()."',
        description = '".$this->get_description()."'
        WHERE   id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function get(){
        $this->sql = "
        SELECT * FROM genders
        WHERE         name LIKE '%".$this->get_name()."%'";
        $this->Squery();
    }
    ##################### FIN #########################
    
    ########################## CONSTRUCTOR Y DESTRUCTOR ################################
    function __construct(){
        $this->db_name = 'library';
    }
    function __destruct(){
        unset($this);
    }
    ##################### FIN #########################
}
?>