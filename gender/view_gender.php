<?php 
$dictionary = [
    'form' => [
        URL_ADD => "/library/".GENDER.URL_ADD,
        URL_MODIFY => "/library/".GENDER.URL_MODIFY,
        URL_EDIT => "/library/".GENDER.URL_EDIT
    ],
    'a_href' => [
        'house_url_'.URL_ADD => PUBLISHING_HOUSE.URL_ADD.'/',
        'house_url_'.URL_MODIFY => PUBLISHING_HOUSE.URL_MODIFY.'/',
        'gender_url_'.URL_ADD => GENDER.URL_ADD.'/',
        'gender_url_'.URL_MODIFY => GENDER.URL_MODIFY.'/',
        'author_url_'.URL_ADD => AUTHOR.URL_ADD.'/',
        'author_url_'.URL_MODIFY => AUTHOR.URL_MODIFY.'/',
        'book_url_'.URL_ADD => BOOK.URL_ADD.'/',
        'book_url_'.URL_MODIFY => BOOK.URL_MODIFY.'/'
    ],
     'general' => [
        'subtitle' => 'Categorias'
    ]
];

function get_template($dir){
    $file = '../site_media/html/'.$dir.'.html';
    $html = file_get_contents($file);
    return $html;
}

function replace($html,$array = []){
    foreach ($array as $key => $value) {
        $html = str_replace('{'.$key.'}',$value,$html);
    }
    return $html;
}
function return_view($dir,$data = []){
    global $dictionary;
    $html = get_template('base');
    $html = replace ($html,$dictionary['general']);
    $html = str_replace('{form}',get_template($dir.'_gender'),$html);
    $html = replace($html,$dictionary['a_href']);
    $html = replace($html,$dictionary['form']);
    $html = str_replace('{content}',dinamic_table_edit($data),$html);
    if(count($data)>0){
        if(array_key_exists('id',$data[0])){
            $html = replace($html,$data[0]);
        }
    }
    print($html);
}
function dinamic_table_edit($data = []){
    $table ='';
    foreach ($data as $key) {
        $table .='<tr>';
        foreach ($key as $key2) {
            $table.='<td>'.$key2.'</td>';
        }
        $table .= '<td><button type="submit" class="btn btn-info" name = "id" value ="'.$key['id'].'">Mod</button></td>';
        $table .= '<td><button type="submit" class="btn btn-danger" name ="id_delete" value ="'.$key['id'].'">Eli</button></td>';
        $table .='</tr>';
    }
    return $table;
}
?>