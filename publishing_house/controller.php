<?php
require_once ('../publishing_house/publishing_house_model.php');
require_once('../core/constant.php');
require_once('../publishing_house/view_house.php');
function start(){
    $request = URL_ADD;
    $url = $_SERVER['REQUEST_URI'];
    $events = [VIEW_ADD, SET, DELETE,
               SHOW_EDIT, URL_MODIFY, URL_ADD,
               URL_EDIT];
    $models = [PUBLISHING_HOUSE];

    foreach ($events as $event) {
        foreach ($models as $model) {
            $url_event = $model.$event;
            if(strpos($url,$url_event) == true){
                $request = $url_event;
            }
        }
    }
    $obj_house = obj();
    $data = helper_publishing_house();
    switch ($request){
        case PUBLISHING_HOUSE.URL_ADD:
            if(isset($data['name'])){
                $obj_house->set_name($data['name']);
                $obj_house->set();
                return_view(URL_ADD);
            }else{
                return_view(URL_ADD);
            }
        break;
        case PUBLISHING_HOUSE.URL_MODIFY:
            if(isset($data['id']) && isset($data['name'])){
                $obj_house->set_name($data['name']);
                $obj_house->set_id($data['id']);
                $obj_house->update();
                $obj_house->set_name('');
                $obj_house->get();
                return_view(URL_MODIFY,$obj_house->get_data());
            }else if(isset($data['name'])){
                $obj_house->set_name($data['name']);
                $obj_house->get();
                return_view(URL_MODIFY,$obj_house->get_data());
            }else{
                $obj_house->set_name('');
                $obj_house->get();
                return_view(URL_MODIFY,$obj_house->get_data());
            }
        break;
        case PUBLISHING_HOUSE.URL_EDIT:
            if(isset($data['id'])){
                $obj_house->set_id($data['id']);
                $obj_house->edit();
                return_view(URL_EDIT,$obj_house->get_data());
            }else if(isset($data['id_delete'])){    
                $obj_house->set_id($data['id_delete']);
                $obj_house->delete();
                $obj_house->set_name('');
                $obj_house->get();
                return_view(URL_MODIFY,$obj_house->get_data());
            }else{
                $obj_house->set_name('');
                $obj_house->get();
                return_view(URL_MODIFY,$obj_house->get_data());
            }
        break;
        default:
            return_view($request);
        break;
    }
}
function obj(){
    $obj = new publishing_model();
    return $obj;
}
function helper_publishing_house(){
    $array = [];
    if($_POST){
        if(isset($_POST['name']) && $_POST['name'] != ''){
            $array['name'] = $_POST['name'];
        }
        if(isset($_POST['id']) && $_POST['id'] != ''){
            $array['id'] = $_POST['id'];
        }
        if(isset($_POST['id_delete']) && $_POST['id_delete'] != ''){
            $array['id_delete'] = $_POST['id_delete'];
        }
    }
    return $array;
}
start();
?>