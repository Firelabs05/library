<?php
require_once ('../core/config.php');
class publishing_model extends database {
    private $name;
    private $id;
    
    #METODOS GET Y SET DE LAS PROPIEDADES
    public function set_name($data){
        $this->name =$data;
    }
    public function get_name(){
        return $this->name;
    }
    public function get_data(){
        return $this->data;
    }
    public function set_id($id){
        $this->id = $id;
    }
    public function get_id(){
        return $this->id;
    }

    # METODOS GET Y SET DE LA BASE DE DATOS
    public function set(){
        $this->sql = "
        INSERT INTO     publishing_house SET 
        name = '".$this->get_name()."'";
        $this->IUDquery();
    }
    public function delete(){
        $this->sql = "
        DELETE FROM publishing_house
        WHERE           id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function edit(){
        $this->sql = "
        SELECT * FROM publishing_house
        WHERE       id ='".$this->get_id()."'";
        $this->Squery();
    }
    public function update(){
        $this->sql = "
        UPDATE publishing_house SET
        name = '".$this->get_name()."'
        WHERE   id='".$this->get_id()."'";
        $this->IUDquery();
    }
    public function get(){
        $this->sql = "
        SELECT * FROM publishing_house
        WHERE         name LIKE '%".$this->get_name()."%'";
        $this->Squery();
    }
    function __construct(){
        $this->db_name = 'library';
    }
    function __destruct(){
        unset($this);
    }
}
?>